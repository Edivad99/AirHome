DROP DATABASE IF EXISTS AirHome;
CREATE DATABASE AirHome;

USE AirHome;

CREATE TABLE IF NOT EXISTS NotificheUWP(
  `ID` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `IDapp` char(36) NOT NULL,
  `URL` text NOT NULL,
  `Registrazione` datetime NOT NULL,
  `Aggiornamento` datetime NULL,
  `n_volte` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS Users(
  `ID` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user` text NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Users` (`user`, `password`) VALUES
('admin', 'admin');

CREATE TABLE IF NOT EXISTS personalRange(
  `Impostazione` varchar(20) PRIMARY KEY NOT NULL,
  `Valore` text NOT NULL,
  `Valore_AUTO` text DEFAULT NULL,
  `Modifica` date DEFAULT '2000-01-01',
  `Notifica` char(2) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `personalRange` (`Impostazione`, `Valore`) VALUES
('Temperatura_min', 'Auto'),
('Temperatura_max', 'Auto'),
('Umidita_min', 'Auto'),
('Umidita_max', 'Auto'),
('Press_min', 'Auto'),
('Press_max', 'Auto');

CREATE TABLE IF NOT EXISTS Impostazioni(
  `Impostazione` varchar(20) PRIMARY KEY NOT NULL,
  `Valore` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Impostazioni` (`Impostazione`, `Valore`) VALUES
('Temp_Notify', 'SI'),
('Umid_Notify', 'SI'),
('Pres_Notify', 'SI'),
('CO2_Notify', 'SI');

CREATE TABLE IF NOT EXISTS Misurazioni(
  `ID` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `Temperatura` float(11) NOT NULL,
  `Umidita` float(11) NOT NULL,
  `Pressione` int(11) NOT NULL,
  `CO2` float(11) NOT NULL,
  `Ora` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
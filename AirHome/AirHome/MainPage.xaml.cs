﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using AirHome.Class;
using System.Diagnostics;
using AirHome.Schermate;
using Windows.Storage;
using Windows.UI.ViewManagement;
// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x410

namespace AirHome
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void myNavView_Loaded(object sender, RoutedEventArgs e)
        {
            myNavView.IsBackButtonVisible = NavigationViewBackButtonVisible.Collapsed;
            myNavView.MenuItems.Add(new NavigationViewItem
            {
                Content = "Home",
                Icon = new SymbolIcon(Symbol.Home),
                Tag = "Home"
            });

            myNavView.MenuItems.Add(new NavigationViewItemSeparator());
           
            var chartsSymbol = Symbol.ThreeBars;

            myNavView.MenuItems.Add(new NavigationViewItem
            {
                Content = "Temperatura",
                Icon = new SymbolIcon(chartsSymbol),
                Tag = "Chart"
            });
            myNavView.MenuItems.Add(new NavigationViewItem
            {
                Content = "Umidità",
                Icon = new SymbolIcon(chartsSymbol),
                Tag = "Chart"
            });
            myNavView.MenuItems.Add(new NavigationViewItem
            {
                Content = "Pressione",
                Icon = new SymbolIcon(chartsSymbol),
                Tag = "Chart"
            });
            myNavView.MenuItems.Add(new NavigationViewItem
            {
                Content = "CO2",
                Icon = new SymbolIcon(chartsSymbol),
                Tag = "Chart"
            });
            myNavView.MenuItems.Add(new NavigationViewItemSeparator());
            myNavView.MenuItems.Add(new NavigationViewItem
            {
                Content = "Cerca",
                Icon = new SymbolIcon(Symbol.Find),
                Tag = "Cerca"
            });
            myNavView.AlwaysShowHeader = true;
            myNavView.SelectedItem = myNavView.MenuItems[0];
            //foreach (NavigationViewItemBase item in myNavView.MenuItems)
            //{
            //    if (item is NavigationViewItem && item.Tag.ToString() == "Home")
            //    {
            //        myNavView.SelectedItem = item;
            //        break;
            //    }
            //}
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values["ip"] == null)
            {
                ContentFrame.Navigate(typeof(Settings));
            }
            else
            {
                ContentFrame.Navigate(typeof(Home));
            }
        }

        private void myNavView_SelectionChanged(NavigationView sender, NavigationViewSelectionChangedEventArgs args)
        {
            //tag
            if (args.IsSettingsSelected)
            {
                ContentFrame.Navigate(typeof(Settings));
                myNavView.Header = "Impostazioni";
            }
            else
            {

                NavigationViewItem item = args.SelectedItem as NavigationViewItem;

                switch (item.Tag)
                {
                    case "Home":
                        ContentFrame.Navigate(typeof(Home));
                        myNavView.Header = "Home";
                        break;

                    case "Temperatura":
                        ContentFrame.Navigate(typeof(Temperature_Chart));
                        myNavView.Header = "Temperatura";
                        break;
                    case "Cerca":
                        ContentFrame.Navigate(typeof(Ricerca));
                        myNavView.Header = "Ricerca";
                        break;
                    case "Umidità":
                        ContentFrame.Navigate(typeof(Humidity_Chart));
                        myNavView.Header = "Umidità";
                        break;
                    case "Pressione":
                        ContentFrame.Navigate(typeof(Pressure_Chart));
                        myNavView.Header = "Pressione";
                        break;
                    case "CO2":
                        ContentFrame.Navigate(typeof(CO2_Chart));
                        myNavView.Header = "CO2";
                        break;
                }
            }

        }

        private void myNavView_ItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            //content

            if (args.IsSettingsInvoked)
            {
                ContentFrame.Navigate(typeof(Settings));
                myNavView.Header = "Impostazioni";
            }
            else
            {
                switch (args.InvokedItem)
                {
                    case "Home":
                        ContentFrame.Navigate(typeof(Home));
                        myNavView.Header = "Home";
                        break;

                    case "Temperatura":
                        ContentFrame.Navigate(typeof(Temperature_Chart));
                        myNavView.Header = "Temperatura";
                        break;
                    case "Cerca":
                        ContentFrame.Navigate(typeof(Ricerca));
                        myNavView.Header = "Ricerca";
                        break;
                    case "Umidità":
                        ContentFrame.Navigate(typeof(Humidity_Chart));
                        myNavView.Header = "Umidità";
                        break;
                    case "Pressione":
                        ContentFrame.Navigate(typeof(Pressure_Chart));
                        myNavView.Header = "Pressione";
                        break;
                    case "CO2":
                        ContentFrame.Navigate(typeof(CO2_Chart));
                        myNavView.Header = "CO2";
                        break;
                }
            }
        }        
    }
}

﻿using AirHome.Class;
using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Home : Page
    {
        public SeriesCollection SeriesCollectionTemp { get; set; }
        public string[] LabelsTemp { get; set; }

        public SeriesCollection SeriesCollectionUmid { get; set; }
        public string[] LabelsUmid { get; set; }

        public SeriesCollection SeriesCollectionPress { get; set; }
        public string[] LabelsPress { get; set; }

        public SeriesCollection SeriesCollectionCO2 { get; set; }
        public string[] LabelsCO2 { get; set; }

        ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        bool errore_rete = false;

        public Home()
        {
            this.InitializeComponent();
            if(localSettings.Values["ip"]!=null)
            {
                GraficoTemp();
                GraficoUmid();
                GraficoPress();
                GraficoCO2();
            }
            else
            {
                var d = new MessageDialog("Devi configurare l'IP del server prima di poter usare questa app!","Configura IP").ShowAsync();
            }
        }

        public async void GraficoTemp()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20Temperature();

            if (result != null)
            {
                if(result.Count>=10)
                {
                    for (int i = result.Count - 10; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].Temperatura);
                    }
                }
                else
                {
                    for (int i = 0; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].Temperatura);
                    }
                }

                SeriesCollectionTemp = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Temperatura",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore linea
                    }
                };

                LabelsTemp = Array.Empty<string>();
                Temperatura.DataContext = this;
            }
            else
            {
                if(!errore_rete)
                {
                    errore_rete = true;
                    var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
                }
            }
        }

        public async void GraficoUmid()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20Humidity();

            if (result != null)
            {
                if (result.Count >= 10)
                {
                    for (int i = result.Count - 10; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].Umidita);
                    }
                }
                else
                {
                    for (int i = 0; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].Umidita);
                    }
                }

                SeriesCollectionUmid = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Umidità",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.IndianRed)//colore linea
                    }
                };
                
                LabelsUmid = Array.Empty<string>();
                Umidita.DataContext = this;
            }
            else
            {
                if (!errore_rete)
                {
                    var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
                    errore_rete = true;
                }
            }
        }

        public async void GraficoPress()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20Pressure();

            if (result != null)
            {
                if (result.Count >= 10)
                {
                    for (int i = result.Count - 10; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].Pressione);
                    }
                }
                else
                {
                    for (int i = 0; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].Pressione);
                    }
                }
                SeriesCollectionPress = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Pressione",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.Indigo)//colore linea
                    }
                };

                LabelsPress = Array.Empty<string>();
                Pressione.DataContext = this;
            }
            else
            {
                if (!errore_rete)
                {
                    var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
                    errore_rete = true;
                }
            }
        }

        public async void GraficoCO2()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20CO2();

            if (result != null)
            {
                if (result.Count >= 10)
                {
                    for (int i = result.Count - 10; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].CO2);
                    }
                }
                else
                {
                    for (int i = 0; i < result.Count; i++)//Prendo gli ultimi 10 risultati
                    {
                        lst.Add(result[i].CO2);
                    }
                }

                SeriesCollectionCO2 = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "CO2",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.LightGreen)//colore linea
                    }
                };

                LabelsCO2 = Array.Empty<string>();
                CO2.DataContext = this;
            }
            else
            {
                if (!errore_rete)
                {
                    var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
                    errore_rete = true;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using AirHome.Class;
using LiveCharts;
using LiveCharts.Uwp;
using Windows.UI.Popups;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Ricerca : Page
    {
        AirHomeData dt;
        public Ricerca()
        {
            this.InitializeComponent();
            CaricaDatePicker();
        }

        public async void CaricaDatePicker()
        {
            dt = new AirHomeData();
            var result = await dt.IntervalYear();
            dtpicker.MinYear = new DateTimeOffset(result.MinYear, 1, 1, 0, 0, 0, TimeSpan.Zero);
            dtpicker.MaxYear = new DateTimeOffset(result.MaxYear, 1, 1, 0, 0, 0, TimeSpan.Zero);
        }
        
        public SeriesCollection SeriesCollectionTemp { get; set; }
        public string[] LabelsTemp { get; set; }
        public SeriesCollection SeriesCollectionUmid { get; set; }
        public string[] LabelsUmid { get; set; }
        public SeriesCollection SeriesCollectionPress { get; set; }
        public string[] LabelsPress { get; set; }
        public SeriesCollection SeriesCollectionCO2 { get; set; }
        public string[] LabelsCO2 { get; set; }

        private void btnCerca_Click(object sender, RoutedEventArgs e)
        {
            string [] data = dtpicker.Date.ToString().Split(" ")[0].Split("/");
            string finalData = String.Format("{0}/{1}/{2}", data[2], data[1], data[0]);
            Crea_Grafico24H(finalData);
        }

        public async void Crea_Grafico24H(string data)
        {

            ChartValues<double> lstTemp = new ChartValues<double>();
            ChartValues<double> lstUmid = new ChartValues<double>();
            ChartValues<double> lstPress = new ChartValues<double>();
            ChartValues<double> lstCO2 = new ChartValues<double>();
            var result = await dt.Cerca(data);

            if (result != null)
            {
                if(result.Count != 0)
                {
                    string[] ora = new string[result.Count];

                    for (int i = 0; i < result.Count; i++)
                    {
                        lstTemp.Add(result[i].Temperatura);
                        lstUmid.Add(result[i].Umidita);
                        lstPress.Add(result[i].Pressione);
                        lstCO2.Add(result[i].CO2);
                        ora[i] = "Ore: " + result[i].Ora;
                    }

                    SeriesCollectionTemp = new SeriesCollection
                    {
                        new LineSeries
                        {
                            Title = "Temperatura",
                            Values = lstTemp,
                            Fill = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore sotto linea
                            Stroke = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore linea
                        }
                    };
                    SeriesCollectionUmid = new SeriesCollection
                    {
                        new LineSeries
                        {
                            Title = "Umidità",
                            Values = lstUmid,
                            Fill = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore sotto linea
                            Stroke = new SolidColorBrush(Windows.UI.Colors.IndianRed),//colore linea
                        }
                    };
                    SeriesCollectionPress = new SeriesCollection
                    {
                        new LineSeries
                        {
                            Title = "Pressione",
                            Values = lstPress,
                            Fill = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore sotto linea
                            Stroke = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore linea
                        }
                    };
                    SeriesCollectionCO2 = new SeriesCollection
                    {
                        new LineSeries
                        {
                            Title = "CO2",
                            Values = lstCO2,
                            Fill = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore sotto linea
                            Stroke = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore linea
                        }
                    };
                    LabelsTemp = LabelsUmid = LabelsPress = LabelsCO2 = ora;
                    Temperatura.Visibility = Umidita.Visibility = Pressione.Visibility = CO2.Visibility = Visibility.Visible;
                    Temperatura.DataContext = null;
                    Temperatura.DataContext = this;
                    Umidita.DataContext = null;
                    Umidita.DataContext = this;
                    Pressione.DataContext = null;
                    Pressione.DataContext = this;
                    CO2.DataContext = null;
                    CO2.DataContext = this;
                }
                else
                {
                    Temperatura.Visibility = Visibility.Collapsed;
                    Umidita.Visibility = Visibility.Collapsed;
                    Pressione.Visibility = Visibility.Collapsed;
                    CO2.Visibility = Visibility.Collapsed;
                    var messageDialog = new MessageDialog("Storico non disponibile per questa data").ShowAsync();
                }
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }

        }
    }
}

﻿using AirHome.Class;
using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Pressure_Chart : Page
    {
        public SeriesCollection SeriesCollectionLast20 { get; set; }
        public string[] LabelsLast20 { get; set; }

        public SeriesCollection SeriesCollection24h { get; set; }
        public string[] Labels24h { get; set; }

        public Pressure_Chart()
        {
            this.InitializeComponent();
            Crea_GraficoLast20();
            Crea_Grafico24H();
            Statistica();
        }

        public async void Crea_GraficoLast20()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20Pressure();

            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].Pressione);
                    ora[i] = result[i].Differenza();
                }

                SeriesCollectionLast20 = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Pressione",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore linea
                    }
                };

                LabelsLast20 = ora;
                Last20.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }
        }

        public async void Crea_Grafico24H()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.Pressure24H();
            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].Pressione);
                    ora[i] = "Ore: " + result[i].Ora.Hour;
                }

                SeriesCollection24h = new SeriesCollection
                {
                    new ColumnSeries
                    {
                        Title = "Pressione",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.Indigo),//colore linea
                    }
                };

                Labels24h = ora;
                if (ora.Length != 0)
                {
                    CultureInfo ci = Thread.CurrentThread.CurrentCulture;
                    giorno24h.Text = "Ultime 24 ore: " + result[0].Ora.ToLongDateString();
                }

                h24.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }
        }

        public async void Statistica()
        {
            AirHomeData data = new AirHomeData();
            var x = await data.GetStatisticaPressione();
            if (x != null)
            {
                if (x.Pres_media_precedente != null)
                {
                    P_med_pas.Text = x.Pres_media_precedente.ToString().Replace('.', ',') + " Pa";
                }
                else
                    P_med_pas.Text = "N/D";

                P_max.Text = x.Pres_max.ToString() + " Pa";
                P_min.Text = x.Pres_min.ToString() + " Pa";
                P_med.Text = x.Pres_media.ToString() + " Pa";
            }
        }
    }
}

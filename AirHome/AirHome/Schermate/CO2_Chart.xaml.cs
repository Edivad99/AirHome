﻿using AirHome.Class;
using LiveCharts;
using LiveCharts.Uwp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class CO2_Chart : Page
    {
        public SeriesCollection SeriesCollectionLast20 { get; set; }
        public string[] LabelsLast20 { get; set; }

        public SeriesCollection SeriesCollection24h { get; set; }
        public string[] Labels24h { get; set; }

        public CO2_Chart()
        {
            this.InitializeComponent();
            Crea_GraficoLast20();
            Crea_Grafico24H();
            Statistica();
        }

        public async void Crea_GraficoLast20()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20CO2();

            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].CO2);
                    ora[i] = result[i].Differenza();
                }

                SeriesCollectionLast20 = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "CO2",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore linea
                    }
                };

                LabelsLast20 = ora;
                Last20.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }
        }

        public async void Crea_Grafico24H()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.CO224H();
            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].CO2);
                    ora[i] = "Ore: " + result[i].Ora.Hour;
                }

                SeriesCollection24h = new SeriesCollection
                {
                    new ColumnSeries
                    {
                        Title = "CO2",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.LightGreen),//colore linea
                    }
                };

                Labels24h = ora;
                if (ora.Length != 0)
                {
                    CultureInfo ci = Thread.CurrentThread.CurrentCulture;
                    giorno24h.Text = "Ultime 24 ore: " + result[0].Ora.ToLongDateString();
                }

                h24.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }
        }

        public async void Statistica()
        {
            AirHomeData data = new AirHomeData();
            var x = await data.GetStatisticaCO2();
            if (x != null)
            {
                if (x.CO2_media_precedente != null)
                {
                    C_med_pas.Text = x.CO2_media_precedente.ToString().Replace('.', ',') + " ppm";
                }
                else
                    C_med_pas.Text = "N/D";

                C_max.Text = x.CO2_max.ToString() + " ppm";
                C_min.Text = x.CO2_min.ToString() + " ppm";
                C_med.Text = x.CO2_media.ToString() + " ppm";
            }
        }
    }
}

﻿using AirHome.Class;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using LiveCharts;
using LiveCharts.Uwp;
using System.Diagnostics;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Popups;
using Windows.Storage;
using System.Globalization;
using System.Threading;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=234238

namespace AirHome.Schermate
{
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class Temperature_Chart : Page
    {

        public SeriesCollection SeriesCollectionLast20 { get; set; }
        public string[] LabelsLast20 { get; set; }

        public SeriesCollection SeriesCollection24h { get; set; }
        public string[] Labels24h { get; set; }

        public async void Crea_GraficoLast20()
        {
            AirHomeData dt = new AirHomeData();
            
            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.GetLast20Temperature();

            if(result!=null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].Temperatura);
                    ora[i] = result[i].Differenza();
                }

                SeriesCollectionLast20 = new SeriesCollection
                {
                    new LineSeries
                    {
                        Title = "Temperatura",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore linea
                    }
                };

                LabelsLast20 = ora;
                //Labels = new[] { "20 min fa", "19 min fa", "18 min fa", "17 min fa", "16 min fa", "15 min fa", "14 min fa", "13 min fa", "12 min fa", "11 min fa", "10 min fa", "9 min fa", "8 min fa", "7 min fa", "6 min fa", "5 min fa", "4 min fa", "3 min fa", "2 min fa", "1 min fa" };
                //YFormatter = value => value.ToString("C");


                //modifying any series values will also animate and update the chart
                //SeriesCollection[0].Values.Add(5d);

                Last20.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }
            
        }

        public async void Crea_Grafico24H()
        {
            AirHomeData dt = new AirHomeData();

            ChartValues<double> lst = new ChartValues<double>();

            var result = await dt.Temperature24H();
            if (result != null)
            {
                string[] ora = new string[result.Count];
                for (int i = 0; i < result.Count; i++)
                {
                    lst.Add(result[i].Temperatura);
                    ora[i] = "Ore: " + result[i].Ora.Hour;
                }

                SeriesCollection24h = new SeriesCollection
                {
                    new ColumnSeries
                    {
                        Title = "Temperatura",
                        Values = lst,
                        Fill = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore sotto linea
                        Stroke = new SolidColorBrush(Windows.UI.Colors.SkyBlue),//colore linea
                    }
                };

                Labels24h = ora;
                if (ora.Length != 0)
                {
                    CultureInfo ci = Thread.CurrentThread.CurrentCulture;
                    giorno24h.Text = "Ultime 24 ore: " + result[0].Ora.ToLongDateString();
                }
                
                h24.DataContext = this;
            }
            else
            {
                var messageDialog = new MessageDialog("Errore di rete").ShowAsync();
            }

        }

        public Temperature_Chart()
        {
            this.InitializeComponent();
            var localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values["ip"] != null)
            {
                Crea_GraficoLast20();
                Crea_Grafico24H();
                //TestAsync();
                Statistica();
            }
            else
            {
                var d = new MessageDialog("Devi configurare l'IP del server prima di poter usare questa app!", "Configura IP").ShowAsync();
            }
        }

        public async void Statistica()
        {
            AirHomeData data = new AirHomeData();
            var x = await data.GetStatisticaTemperatura();
            if(x!=null)
            {
                if (x.Temp_media_precedente != null)
                {
                    Uri uri;
                    if (x.Temp_media >= Convert.ToDouble(x.Temp_media_precedente.Replace('.',',')))
                        uri = new Uri("ms-appx:///Assets/img/Green_Arrow.png");
                    else
                        uri = new Uri("ms-appx:///Assets/img/Red_Arrow.png");
                    T_status.Source = new BitmapImage(uri);
                    T_med_pas.Text = x.Temp_media_precedente.ToString().Replace('.',',') + "°C";
                }
                else
                    T_med_pas.Text = "N/D";

                T_max.Text =  x.Temp_max.ToString() + "°C";
                T_min.Text = x.Temp_min.ToString() + "°C";
                T_med.Text = x.Temp_media.ToString() + "°C";
                
            }

            var meteo = await data.MeteoEsterno();
            T_max_est.Text = meteo.Temperatura_Massima;
            T_min_est.Text = meteo.Temperatura_Minima;
            
        }
        public async void TestAsync()
        {
            AirHomeData data = new AirHomeData();

            var x = await data.MeteoEsterno();

            Debug.WriteLine(x.Temperatura);


            //var x = await data.GetLast20Humidity();

            //Debug.WriteLine(x[0].ToString());

            //var y = await data.GetLast20Pressure();

            //Debug.WriteLine(y[0].ToString());

            //var z = await data.GetLast20CO2();

            //Debug.WriteLine(z[0].ToString());



            //MeteoEsternoClass m = new MeteoEsternoClass();

            //m.GetMeteoEsterno();

            //Debug.WriteLine(x.report.location.city);

        }
    }
}

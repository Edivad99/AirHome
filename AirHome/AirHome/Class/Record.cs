﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirHome.Class
{
    class Record
    {
        public DateTime Ora { get; set; }

        public string Differenza()
        {
            DateTime adesso = DateTime.Now;

            TimeSpan risultato = adesso.Subtract(Ora);
            string ris;

            if (risultato.Days > 0)
            {
                if(risultato.Hours > 0)
                {
                    ris = String.Format("{0} giorno/i, {1} ora/e, {2} minuto/i fa", risultato.Days, risultato.Hours, risultato.Minutes);
                }
                else
                {
                    ris = String.Format("{0} giorno/i,{1} minuto/i fa", risultato.Days, risultato.Minutes);
                }
            }
            else
            {
                if (risultato.Hours > 0)
                {
                    ris = String.Format("{0} ora/e, {1} minuto/i fa", risultato.Hours, risultato.Minutes);
                }
                else
                {
                    if (risultato.Minutes > 1)
                        ris = String.Format("{0} minuti fa", risultato.Minutes);
                    else if (risultato.Minutes == 1)
                        ris = String.Format("{0} minuto fa", risultato.Minutes);
                    else
                        ris = "Adesso";
                }
            }

            return ris;
        }
    }

    class MeteoEsterno
    {
        public string Temperatura_Minima { get; set; }
        public string Temperatura_Massima { get; set; }
        public string Temperatura { get; set; }
        public string Umidita { get; set; }
        public string Pioggia { get; set; }
        public string Nuvole { get; set; }
        public string Pressione { get; set; }
        public string Alba { get; set; }
        public string Tramonto { get; set; }
        public string Luna { get; set; }
        public string Luna_immagine { get; set; }
        public string Vento { get; set; }
        public string Vento_immagine { get; set; }
        public string Vento_direzione { get; set; }
        public string Condizione_immagine { get; set; }
        public string Descrizione { get; set; }
        public string Last_Update { get; set; }

        public MeteoEsterno(string temperatura_Minima, string temperatura_Massima, string temperatura, string umidita, string pioggia, string nuvole, string pressione, string alba, string tramonto, string luna, string luna_immagine, string vento, string vento_immagine, string vento_direzione, string condizione_immagine, string descrizione, string last_Update)
        {
            Temperatura_Massima = temperatura_Massima;
            Temperatura_Minima = temperatura_Minima;
            Temperatura = temperatura;
            Umidita = umidita;
            Pioggia = pioggia;
            Nuvole = nuvole;
            Pressione = pressione;
            Alba = alba;
            Tramonto = tramonto;
            Luna = luna;
            Luna_immagine = luna_immagine;
            Vento = vento;
            Vento_immagine = vento_immagine;
            Vento_direzione = vento_direzione;
            Condizione_immagine = condizione_immagine;
            Descrizione = descrizione;
            Last_Update = last_Update;
        }
    }

    class Temperature : Record
    {
        public double Temperatura { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Temperatura;
        }
    }

    class Humidity : Record
    {
        public double Umidita { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Umidita;
        }
    }

    class Pressure : Record
    {
        public double Pressione { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Pressione;
        }
    }

    class Co2 : Record
    {
        public double CO2 { get; set; }

        public override string ToString()
        {
            return Ora + " : " + CO2;
        }
    }

    class Misurazione:Record
    {
        public double Temperatura { get; set; }
        public double Umidita { get; set; }
        public double Pressione { get; set; }
        public double CO2 { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Temperatura + " " + Umidita + " " + Pressione + " " + CO2;
        }
    }

    class Cerca
    {
        public double Temperatura { get; set; }
        public double Umidita { get; set; }
        public double Pressione { get; set; }
        public double CO2 { get; set; }
        public int Ora { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Temperatura + " " + Umidita + " " + Pressione + " " + CO2;
        }
    }

    class Statistica:Record
    {
        public double Temp_min { get; set; }
        public double Temp_max { get; set; }
        public double Umid_min { get; set; }
        public double Umid_max { get; set; }
        public int Pres_min { get; set; }
        public int Pres_max { get; set; }
        public double CO2_min { get; set; }
        public double CO2_max { get; set; }

        public override string ToString()
        {
            return string.Format("Temperatura minima: {0}\n" +
                                 "Temperatura massima: {1}\n" +
                                 "Umidita' minima: {2}\n" +
                                 "Umidita' massima: {3}\n" +
                                 "Pressione minima: {4}\n" +
                                 "Pressione massima: {5}\n" +
                                 "CO2 minima: {6}\n" +
                                 "CO2 massima: {7}\n" +
                                 "Rilevazione: {8}", Temp_min, Temp_max, Umid_min, Umid_max, Pres_min, Pres_max, CO2_min, CO2_max, Ora);
        }
    }

    class StatisticaTemperatura:Record
    {
        public double Temp_min { get; set; }
        public double Temp_max { get; set; }
        public double Temp_media { get; set; }
        public string Temp_media_precedente { get; set; }

        public override string ToString()
        {
            return Ora + " " + Temp_min + " " + Temp_max + " " + Temp_media + " "  + (Temp_media_precedente != null ? Temp_media_precedente : "N/D");
        }
    }

    class StatisticaUmidita : Record
    {
        public double Umid_min { get; set; }
        public double Umid_max { get; set; }
        public double Umid_media { get; set; }
        public string Umid_media_precedente { get; set; }

        public override string ToString()
        {
            return Ora + " " + Umid_min + " " + Umid_max + " " + Umid_media + " "  + (Umid_media_precedente != null ? Umid_media_precedente : "N/D");
        }
    }

    class StatisticaPressione : Record
    {
        public double Pres_min { get; set; }
        public double Pres_max { get; set; }
        public double Pres_media { get; set; }
        public string Pres_media_precedente { get; set; }

        public override string ToString()
        {
            return Ora + " " + Pres_min + " " + Pres_max + " " + Pres_media + " " + (Pres_media_precedente != null ? Pres_media_precedente : "N/D");
        }
    }

    class StatisticaCO2 : Record
    {
        public double CO2_min { get; set; }
        public double CO2_max { get; set; }
        public double CO2_media { get; set; }
        public string CO2_media_precedente { get; set; }

        public override string ToString()
        {
            return Ora + " " + CO2_min + " " + CO2_max + " " + CO2_media + " " + (CO2_media_precedente != null ? CO2_media_precedente : "N/D");
        }
    }

    class IntervalYear
    {
        public int MaxYear { get; set; }
        public int MinYear { get; set; }
    }
}

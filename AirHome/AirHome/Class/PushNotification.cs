﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Windows.Networking.NetworkOperators;
using Windows.Networking.PushNotifications;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Profile;
using Windows.UI.Popups;
using Windows.Web.Http;

namespace AirHome.Class
{
    class PushNotification
    {
        //Creare il collegamento per le notifiche push
        public static async void InitializeNotifications()
        {
            var localSettings = ApplicationData.Current.LocalSettings;
            var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

            if (localSettings.Values["ip"] != null && localSettings.Values["IDapp"] != null)
            {
                Debug.WriteLine("IDapp: " + localSettings.Values["IDapp"]);
                SendChannelAsync(localSettings,channel);
            }
        }

        private static async void SendChannelAsync(ApplicationDataContainer localSettings, PushNotificationChannel channel)
        {
            Dictionary<string, string> post = new Dictionary<string, string>();
            post.Add("channel", channel.Uri);
            post.Add("IDapp", localSettings.Values["IDapp"].ToString());
            
            try
            {
                Uri url = new Uri("http://" + localSettings.Values["ip"] + "/php/push_notification/updatechannel.php", UriKind.Absolute);
                HttpFormUrlEncodedContent formContent = new HttpFormUrlEncodedContent(post);
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.PostAsync(url, formContent);
                Debug.WriteLine("Risposta WebServer: " + response.Content);
            }
            catch (Exception)
            {
                var d = new MessageDialog("Internet non disponibile").ShowAsync();
            }
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace AirHome.Class
{
    class AirHomeData
    {
        private const string key = "9c526a3db470c2233929169c69a2c07319f7b868";
        private string baseuri;
        private string meteoEsturi;
        public string Tilexmluri;
        private string silentMode;
        private HttpClient client;
        private ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        public AirHomeData(string ip_fisso = "")
        {
            client = new HttpClient();
            string ip;
            if (ip_fisso == "")
            {
                if (localSettings.Values["ip"] != null)
                    ip = localSettings.Values["ip"].ToString();
                else
                    ip = "192.168.1.150";
            }
            else
            {
                ip = ip_fisso;
            }

            baseuri = "http://" + ip + "/php/queryjson.php?key=" + key + "&qr=";
            meteoEsturi = "http://" + ip + "/php/weatherapi.php?key=" + key;
            Tilexmluri = "http://" + ip + "/php/push_notification/TileXML.php?key=" + key;
            silentMode = "http://" + ip + "/php/push_notification/silentmode.php";
        }


        public enum Query
        {
            Last20Records , Last20Temperature , Last20Humidity,
            Last20Pressure, Last20TCO2, Statistica, StatisticaTemperatura,
            Temperature24H, StatisticaUmidita, Humidity24H, StatisticaPressione,
            Pressure24H, StatisticaCO2, Co224H, IntervalYear,Cerca=101
        }

        public async Task<MeteoEsterno> MeteoEsterno()
        {
            try
            {
                var uri = new Uri(meteoEsturi);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<MeteoEsterno>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Misurazione>> GetLast20Records()
        {
            try
            {
                var uri = new Uri(baseuri + Query.Last20Records);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Misurazione>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Temperature>> GetLast20Temperature()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20Temperature);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Temperature>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Humidity>> GetLast20Humidity()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20Humidity);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Humidity>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Pressure>> GetLast20Pressure()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20Pressure);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Pressure>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Co2>> GetLast20CO2()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20TCO2);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Co2>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<Statistica> GetStatistica()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Statistica);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Statistica>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<StatisticaTemperatura> GetStatisticaTemperatura()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.StatisticaTemperatura);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<StatisticaTemperatura>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Temperature>> Temperature24H()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Temperature24H);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Temperature>>(response);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<StatisticaUmidita> GetStatisticaUmidita()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.StatisticaUmidita);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<StatisticaUmidita>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Humidity>> Humidity24H()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Humidity24H);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Humidity>>(response);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<StatisticaPressione> GetStatisticaPressione()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.StatisticaPressione);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<StatisticaPressione>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Pressure>> Pressure24H()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Pressure24H);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Pressure>>(response);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<StatisticaCO2> GetStatisticaCO2()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.StatisticaCO2);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<StatisticaCO2>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Co2>> CO224H()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Co224H);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Co2>>(response);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        //Cerca
        public async Task<List<Cerca>> Cerca(string data)
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Cerca + "&date=" + data);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Cerca>>(response);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
        //Blocco notifiche
        public async Task<string> SilenziaNotifiche(string date)
        {
            try
            {
                var uri = new Uri(silentMode + "?idapp=" + localSettings.Values["IDapp"] + "&data=" + date);
                var response = await client.GetStringAsync(uri);

                return response;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<IntervalYear> IntervalYear()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.IntervalYear);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<IntervalYear>>(response)[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}

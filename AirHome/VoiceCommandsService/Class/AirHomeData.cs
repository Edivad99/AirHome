﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace AirHome.VoiceCommands.Class
{
    class AirHomeData
    {
        private const string key = "9c526a3db470c2233929169c69a2c07319f7b868";
        private string baseuri;
        private HttpClient client;
        private ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

        public AirHomeData()
        {
            client = new HttpClient();

            baseuri = "http://edivad.myftp.org/php/queryjson.php?key=" + key + "&qr=";
        }


        public enum Query
        {
            Last20Records , Last20Temperature , Last20Humidity,
            Last20Pressure, Last20TCO2
        }

        public async Task<List<Misurazione>> GetLast20Records()
        {
            try
            {
                var uri = new Uri(baseuri + Query.Last20Records);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Misurazione>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Temperature>> GetLast20Temperature()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20Temperature);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Temperature>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Humidity>> GetLast20Humidity()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20Humidity);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Humidity>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Pressure>> GetLast20Pressure()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20Pressure);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Pressure>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }

        public async Task<List<Co2>> GetLast20CO2()
        {
            try
            {
                var uri = new Uri(baseuri + (int)Query.Last20TCO2);
                var response = await client.GetStringAsync(uri);

                return JsonConvert.DeserializeObject<List<Co2>>(response);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}

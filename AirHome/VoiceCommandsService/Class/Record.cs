﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirHome.VoiceCommands.Class
{
    class Record
    {
        public DateTime Ora { get; set; }

        public string Differenza()
        {
            DateTime adesso = DateTime.Now;

            TimeSpan risultato = adesso.Subtract(Ora);
            string ris;

            if (risultato.Days > 0)
            {
                if(risultato.Hours > 0)
                {
                    ris = String.Format("{0} giorno/i, {1} ora/e, {2} minuto/i fa", risultato.Days, risultato.Hours, risultato.Minutes);
                }
                else
                {
                    ris = String.Format("{0} giorno/i,{1} minuto/i fa", risultato.Days, risultato.Minutes);
                }
            }
            else
            {
                if (risultato.Hours > 0)
                {
                    ris = String.Format("{0} ora/e, {1} minuto/i fa", risultato.Hours, risultato.Minutes);
                }
                else
                {
                    if (risultato.Minutes > 1)
                        ris = String.Format("{0} minuti fa", risultato.Minutes);
                    else if (risultato.Minutes == 1)
                        ris = String.Format("{0} minuto fa", risultato.Minutes);
                    else
                        ris = "Adesso";
                }
            }

            return ris;
        }
    }
    

    class Temperature : Record
    {
        public double Temperatura { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Temperatura;
        }
    }

    class Humidity : Record
    {
        public double Umidita { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Umidita;
        }
    }

    class Pressure : Record
    {
        public double Pressione { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Pressione;
        }
    }

    class Co2 : Record
    {
        public double CO2 { get; set; }

        public override string ToString()
        {
            return Ora + " : " + CO2;
        }
    }

    class Misurazione:Record
    {
        public double Temperatura { get; set; }
        public double Umidita { get; set; }
        public double Pressione { get; set; }
        public double CO2 { get; set; }

        public override string ToString()
        {
            return Ora + " : " + Temperatura + " " + Umidita + " " + Pressione + " " + CO2;
        }
    }
}

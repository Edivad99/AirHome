﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.AppService;
using Windows.ApplicationModel.Background;
using Windows.ApplicationModel.Resources.Core;
using Windows.ApplicationModel.VoiceCommands;
using AirHome.VoiceCommands.Class;
namespace AirHome.VoiceCommands
{
    public sealed class AirHomeVoiceCommandService : IBackgroundTask
    {
        VoiceCommandServiceConnection voiceServiceConnection;

        BackgroundTaskDeferral serviceDeferral;

        ResourceMap cortanaResourceMap;

        ResourceContext cortanaContext;
        

        public async void Run(IBackgroundTaskInstance taskInstance)
        {

            BackgroundTaskDeferral serviceDeferral = taskInstance.GetDeferral();


            var triggerDetails = taskInstance.TriggerDetails as AppServiceTriggerDetails;

            if (triggerDetails != null && triggerDetails.Name == "AirHomeVoiceCommandService")
            {
                try
                {
                    voiceServiceConnection = VoiceCommandServiceConnection.FromAppServiceTriggerDetails(triggerDetails);

                    

                    //// GetVoiceCommandAsync establishes initial connection to Cortana, and must be called prior to any 
                    //// messages sent to Cortana. Attempting to use ReportSuccessAsync, ReportProgressAsync, etc
                    //// prior to calling this will produce undefined behavior.
                    VoiceCommand voiceCommand = await voiceServiceConnection.GetVoiceCommandAsync();

                    //// Depending on the operation (defined in AdventureWorks:AdventureWorksCommands.xml)
                    //// perform the appropriate command.
                    switch (voiceCommand.CommandName)
                    {
                        case "showTemperatura":
                            AvvisaTemperatura();
                            break;

                        default:
                            // As with app activation VCDs, we need to handle the possibility that
                            // an app update may remove a voice command that is still registered.
                            // This can happen if the user hasn't run an app since an update.
                            LaunchAppInForeground();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Handling Voice Command failed " + ex.ToString());
                }
            }
        }

        private async void AvvisaTemperatura()
        {
            AirHomeData dt = new AirHomeData();
            
            List<Temperature> x = await dt.GetLast20Temperature();
            await ShowProgressScreen("Sto leggendo la temperatura...");

            string ora = x[x.Count - 1].Ora.Hour + " e " + x[x.Count - 1].Ora.Minute;
            string messagio_parlato = String.Format("La temperatura delle {0} è di {1} gradi", ora, x[x.Count - 1].Temperatura);

            ora = x[x.Count - 1].Ora.Hour + ":" + x[x.Count - 1].Ora.Minute;
            string messagio_scritto = String.Format("La temperatura delle {0} è di {1}°C", ora, x[x.Count - 1].Temperatura);

            var userMessage = new VoiceCommandUserMessage
            {
                DisplayMessage = "AirHome",
                SpokenMessage = messagio_parlato
            };

            var contenuto = new List<VoiceCommandContentTile>();
            var destinationTile = new VoiceCommandContentTile
            {
                ContentTileType = VoiceCommandContentTileType.TitleWithText,
                TextLine1 = messagio_scritto
            };
            contenuto.Add(destinationTile);

            var response = VoiceCommandResponse.CreateResponse(userMessage,contenuto);

            await voiceServiceConnection.ReportSuccessAsync(response);
        }

        private async Task ShowProgressScreen(string message)
        {
            var userProgressMessage = new VoiceCommandUserMessage();
            userProgressMessage.DisplayMessage = userProgressMessage.SpokenMessage = message;

            VoiceCommandResponse response = VoiceCommandResponse.CreateResponse(userProgressMessage);
            await voiceServiceConnection.ReportProgressAsync(response);
        }


        private void OnTaskCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            Debug.WriteLine("Task cancelled, clean up");
            if (this.serviceDeferral != null)
            {
                //Complete the service deferral
                this.serviceDeferral.Complete();
            }
        }

        private void OnVoiceCommandCompleted(VoiceCommandServiceConnection sender, VoiceCommandCompletedEventArgs args)
        {
            if (this.serviceDeferral != null)
            {
                this.serviceDeferral.Complete();
            }
        }

        private async void LaunchAppInForeground()
        {
            var userMessage = new VoiceCommandUserMessage
            {
                SpokenMessage = cortanaResourceMap.GetValue("LaunchingAdventureWorks", cortanaContext).ValueAsString
            };

            var response = VoiceCommandResponse.CreateResponse(userMessage);

            response.AppLaunchArgument = "";

            await voiceServiceConnection.RequestAppLaunchAsync(response);
        }
    }
}

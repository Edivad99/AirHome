int co2Input = A0;
void setup() {
  // put your setup code here, to run once:
  pinMode(co2Input, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  int t1 = CO2t1();
  int t2 = CO2t2();
  Serial.print("Algoritmo precedente: ");
  Serial.print(t1);
  Serial.print("\t");
  Serial.print("Algoritmo successivo: ");
  Serial.println(t2);
}


int CO2t1()
{
  int mediaco2;
  for (int x = 0; x < 10; x++)
  {
    mediaco2 += analogRead(co2Input);
    delay(200);
  }
  mediaco2 = mediaco2 / 10;
  int ppm = map(mediaco2, 0, 1023, 400, 2100);
  return ppm;
}

int CO2t2()
{
  int Vout = analogRead(co2Input);
  int Rs = 10 * (6204.5 - Vout) / Vout;
  //int Rs = ((5.0 * 10) - (10 * analogRead(co2Input)));
  int ratio = (Rs / 76.63);
  ratio = ratio * 0.3611;
  int ppm = (146.15*(2.868 - ratio) + 10);
  return ppm;
}


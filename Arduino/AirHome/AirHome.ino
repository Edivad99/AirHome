#include <DHT.h>
#include <DHT_U.h>
#include <Wire.h>//Press
#include <BMP180.h>//Press

BMP180 barometer;
DHT dht(12, DHT22);

int led_red = 2;
int led_blue = 4;
int led_green = 3;
int co2Input = A0;
int lightInput = A1;
int intervallosend = 300000;//300000
int intervalloled = 30000;
unsigned long previousMillisSend = 0;
unsigned long previousMillisLed = 0;

void setup()
{
  // put your setup code here, to run once:
  pinMode(led_red, OUTPUT);
  pinMode(led_blue, OUTPUT);
  pinMode(led_green, OUTPUT);
  pinMode(co2Input, INPUT);
  pinMode(lightInput, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.begin(9600);
  Wire.begin();
  barometer = BMP180();
  if (barometer.EnsureConnected())
  {
    //Serial.println("OK");
    barometer.SoftReset();
    barometer.Initialize();
  }
  else
  {
    //Serial.println("ERR");
  }
  dht.begin();
}

void loop()
{
  unsigned long currentMillis = millis();
  // put your main code here, to run repeatedly:
  if ((unsigned long)(currentMillis - previousMillisSend) >= 300000 ) //Invio i dati al raspberry
  {
    //codice da eseguire
    long currentPressure = 0;
    float h;
    float t;
    float t1;
    int mediaco2;
    int ppm;
    if (barometer.IsConnected)
    {
      currentPressure = barometer.GetPressure();//risultato in P
      t1 = barometer.GetTemperature();
    }
    do
    {
      h = dht.readHumidity();
      t = dht.readTemperature();
    } while (isnan(t) || isnan(t));

    t = ((t * 90) + (t1 * 10)) / 100;//media ponderata

    for (int x = 0; x < 10; x++)
    {
      mediaco2 += analogRead(co2Input);
      delay(200);
    }
    mediaco2 = mediaco2 / 10;
    ppm = map(mediaco2, 0, 1023, 400, 2100);
    Serial.print(t);
    Serial.print("#");
    Serial.print(h);
    Serial.print("#");
    Serial.print(currentPressure);
    Serial.print("#");
    Serial.print(ppm);
    Serial.print("#");
    Serial.println(Humidex(t, h));
    for (int i = 0; i < 10; i++)
    {
      digitalWrite(LED_BUILTIN, HIGH);
      delay(100);
      digitalWrite(LED_BUILTIN, LOW);
      delay(100);
    }
    previousMillisSend = currentMillis;
    setLedbyEnviroment(t, h);
  }

  if ((unsigned long)(currentMillis - previousMillisLed) >= intervalloled) //Aggiorno il led
  {
    //codice da eseguire
    long currentPressure = 0;
    float h;
    float t;
    float t1;
    Serial.println("Led");
    if (barometer.IsConnected)
    {
      currentPressure = barometer.GetPressure();//risultato in P
      t1 = barometer.GetTemperature();
    }
    do
    {
      h = dht.readHumidity();
      t = dht.readTemperature();
    } while (isnan(t) || isnan(t));

    t = ((t * 90) + (t1 * 10)) / 100;//media ponderata
    previousMillisLed = currentMillis;
    setLedbyEnviroment(t, h);
  }
}

int Humidex(double temp, double umid)
{
  double kelvin = temp + 273;
  double eTs = pow(10, ((-2937.4 / kelvin) - 4.9283 * log(kelvin) / log(10) + 23.5471));//log = ln
  double eTd = eTs * umid / 100;
  double hx = round(temp + ((eTd - 10) * 5 / 9));
  return int(hx);
}

void setLedbyEnviroment(double temp, double umid)
{
  if (analogRead(lightInput) > 200)
  {
    int humidex = Humidex(temp, umid);
    if (humidex <= 29) //verde
    {
      SetColor(0, 255, 0);
    }
    else if (humidex > 29 && humidex <= 34) //blu
    {
      SetColor(0, 0, 255);
    }
    else if (humidex > 34 && humidex <= 39) //viola
    {
      SetColor(255, 0, 255);
    }
    else if (humidex > 39 && humidex <= 45) //giallo
    {
      SetColor(150, 165, 0);
    }
    else if (humidex > 45 && humidex <= 53) //arancione
    {
      SetColor(255, 255, 0);
    }
    else//rosso
    {
      for (int i = 0; i < 10; i++)
      {
        SetColor(255, 0, 0);
        delay(250);
        SetColor(0, 0, 0);
        delay(250);
      }
    }
  }
  else
  {
    SetColor(0, 0, 0);
  }
}

void SetColor(int r, int g, int b)
{
  analogWrite(led_red, r);
  analogWrite(led_green, g);
  analogWrite(led_blue, b);
}

